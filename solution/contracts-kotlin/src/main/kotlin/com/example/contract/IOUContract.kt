package com.example.contract

import com.example.state.IOUState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.transactions.LedgerTransaction

/**
 * A implementation of a basic smart contract in Corda.
 *
 * This contract enforces rules regarding the creation of a valid [IOUState], which in turn encapsulates an [IOUState].
 *
 * For a new [IOUState] to be issued onto the ledger, a transaction is required which takes:
 * - Zero input states.
 * - One output state: the new [IOUState].
 * - An Create() command with the public keys of both the lender and the borrower.
 *
 * All contracts must sub-class the [Contract] interface.
 */
class IOUContract : Contract {
    companion object {
        @JvmStatic
        val ID = "com.example.contract.IOUContract"
    }

    interface Commands : CommandData {
        class Create : Commands
        class Settle : Commands
    }

    /**
     * The verify() function of all the states' contracts must not throw an exception for a transaction to be
     * considered valid.
     */
    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Commands>()

        when (command.value) {

            is Commands.Create -> requireThat {

                "No input when create an IOU." using (tx.inputs.isEmpty())
                "Only one output state should be created." using (tx.outputs.size == 1)

                val output = tx.outputsOfType<IOUState>().single()
                "The lender and the borrower cannot be the same entity." using (output.lender != output.borrower)
                "The IOU's value must greater than 0." using (output.value > 0)

            }

            is Commands.Settle -> requireThat {

                "One input when settle an IOU." using (tx.inputs.size == 1)
                "One output when settle an IOU." using (tx.outputs.size == 1)

                val input = tx.inputsOfType<IOUState>().single()
                val output = tx.outputsOfType<IOUState>().single()
                "The lender and the borrower cannot be the same entity." using (output.lender != output.borrower)
                "The IOU's value must be non-negative." using (output.value >= 0)
                "Lender input same with Lender output." using (input.lender == output.lender)
                "Borrower input same with Borrower output." using (input.borrower == output.borrower)
            }

        }
    }


}
